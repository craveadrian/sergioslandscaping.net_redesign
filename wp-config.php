<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'exam_sergioslandscaping.net' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '7FkeYz_k}`2zgVdphPtD&*Z5TE^|]+#qym#iJ^|~C4xteS0<Pk~8Z_K>ulhtWgf<' );
define( 'SECURE_AUTH_KEY',  '2Fw~w($B@&[ISX6sn{<$#Oy5Kv]X|g~-++[VRT&SfuYa(,~X3/drM5*J/;zzCeda' );
define( 'LOGGED_IN_KEY',    '_],%+glSftxMw}9_XjwWq/T+dLJW0OK#seZ@/?!*.Tip$F8`DW-*l;,w.X2*5R)c' );
define( 'NONCE_KEY',        '&@?).Ji,(#LZqX8oH 6#|XiPfv]D(0+drP&rBipY,!;wIq&6||W([H8Et0rBZMsG' );
define( 'AUTH_SALT',        'xe#R%yT41PJ><8=xyWU *y=0*i4=i|eps4$K.qU>Ty$OpPqb*?l)yBbbU~4dIc;1' );
define( 'SECURE_AUTH_SALT', '0QL~df$}7}rkeHzP/edf0-ZXeD7^IyflIP66:|U;7gZqRyvu]%5<HeSD,c7f*vxY' );
define( 'LOGGED_IN_SALT',   '(Kwgcb+7[4^9;y!.enQ-I|+Ffb{W7 XZQ9&Em?sWxgN.64}--fMJ,}r9NdWfGe&^' );
define( 'NONCE_SALT',       'T~.jpJ$NFNGum>BUoiCwxEKNQ$c>ly<$L6TSjDp}`R.H|9wX}?ip8#W1p_DmJtfQ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
