<?php
/**
 * Displays footer site page bottom area
 *
 * @package WordPress
 * @subpackage WP5_Default
 * @since 1.0.0
 */
?>
<?php if ( is_active_sidebar( 'site-pagebottom' ) ) : ?>
<div class="site-page-bottom">

	<?php dynamic_sidebar( 'site-pagebottom' ); ?>
	<div class="social-media">
		<?php get_template_part( 'template-parts/navigation/navigation', 'social' ); ?>
	</div>
</div><!-- .site-banner -->
<?php endif; ?>
