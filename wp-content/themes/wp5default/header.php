<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage WP5_Default
 * @since 1.0.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.png" type="image/x-icon"/>
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.png" type="image/x-icon"/>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'wp5default' ); ?></a>

	<header id="masthead" class="site-header">

		<div class="site-branding-container container">
			<?php //get_template_part( 'template-parts/header/site', 'branding' ); ?>
			<div class="hd-wrap">
				<?php if ( has_custom_logo() ) : ?>
					<div class="hd-logo   animated slideInLeft delay1 duration1 "><?php the_custom_logo(); ?></div>
				<?php endif; ?>
				<div class="header-nav animated slideInRight delay1 duration1">
					<div class="header-nav-top">
						<div class="phone">
							<p><span>PHONE: </span>
								<?php if ( checkoption( 'phone' ) ):
									echo do_shortcode('[wp5default_option var="phone" type="link" text="602 552 5400" link_type="phone"]');
								endif; ?>
							</p>
						</div>
						<div class="social-media">
							<?php get_template_part( 'template-parts/navigation/navigation', 'social' ); ?>
						</div>
					</div>
					<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
				</div>
			</div>
		</div><!-- .layout-wrap -->

	</header><!-- #masthead -->

	<?php get_template_part( 'template-parts/header/site', 'banner' ); ?>

	<div id="content" class="site-content">
