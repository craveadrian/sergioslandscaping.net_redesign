<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage WP5_Default
 * @since 1.0.0
 */

?>

	</div><!-- #content -->
	<?php get_template_part( 'template-parts/footer/site', 'pagebottom' ); ?>
	<footer id="colophon" class="site-footer  animated slideInRight delay1 duration1 eds-on-scroll ">
		<div class="site-info container">
			<div class="footer-nav">
				<?php get_template_part( 'template-parts/navigation/navigation', 'bottom' ); ?>
			</div>
			<?php get_template_part( 'template-parts/footer/site', 'info' ); ?>

		</div><!-- .site-info -->
	</footer><!-- #colophon -->
	<label for="g-recaptcha-response-100000"><i style="width: 1px; height: 1px; display: block; overflow: hidden;">Response</i></label>
	<textarea id="g-recaptcha-response-100000" style="display: none;"></textarea>

</div><!-- #page -->

<?php wp_footer(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
<script>
  WebFont.load({
    google: {
      families: ['Titillium Web', sans-serif],['Libre Baskerville', serif;],['Montserrat', sans-serif;]
    }
  });
</script>
</body>
</html>
